import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SampleViewComponent, SampleViewHeaderComponent } from './components/sample-view';

// Modules
import { CoreModule } from './core/core.module';

@NgModule({
  declarations: [
    AppComponent,
    SampleViewComponent,
    SampleViewHeaderComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
