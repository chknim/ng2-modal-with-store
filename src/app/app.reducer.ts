import { Action } from '@ngrx/store';

import { SampleViewActions } from './components/sample-view/sample-view.actions';

export interface SampleViewState {
  showSampleView: boolean;
};

const initialState: SampleViewState = {
  showSampleView: false
};

export function sampleViewReducer(state = initialState, action: Action): SampleViewState {
  switch (action.type) {

    case SampleViewActions.SHOW_SAMPLE_VIEW: {
      return Object.assign({}, state, {
        showSampleView: true
      });
    }

    case SampleViewActions.HIDE_SAMPLE_VIEW: {
      return Object.assign({}, state, {
        showSampleView: false
      });
    }

    default: {
      return state;
    }
  }
}
