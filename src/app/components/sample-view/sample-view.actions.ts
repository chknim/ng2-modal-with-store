import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Action } from '@ngrx/store';

@Injectable()
export class SampleViewActions {

  static SHOW_SAMPLE_VIEW = 'SHOW_SAMPLE_VIEW';
  showSampleView(): Action {
    return {
      type: SampleViewActions.SHOW_SAMPLE_VIEW
    }
  }

  static HIDE_SAMPLE_VIEW = 'HIDE_SAMPLE_VIEW';
  hideSampleView(): Action {
    return {
      type: SampleViewActions.HIDE_SAMPLE_VIEW
    }
  }
}
