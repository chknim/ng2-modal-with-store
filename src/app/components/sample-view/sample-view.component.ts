import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sample-view',
  templateUrl: './sample-view.component.html',
  styleUrls: ['./sample-view.component.less']
})
export class SampleViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
