import { Component, OnInit, EventEmitter, Output } from '@angular/core';

// Redux
import { AppState } from '../../reducers';
import { Store } from '@ngrx/store';
import { SampleViewActions } from './sample-view.actions';

@Component({
  selector: 'sample-view-header',
  template: `
    <div *ngIf="!factory" class="container-fluid" onClose="closeNotified($event)">
      <div class="row">
        <div class="col-md-11 title">
          <img src="../../assets/information.svg">
          <span>Your Sample View Header Component</span>
        </div>
        <div class="col-md-1 buttons">
          <button class="btn btn-default btn-icon" (click)="close($event)">X</button>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./sample-view-header.component.less'],
  providers: [
    SampleViewActions
  ]
})
export class SampleViewHeaderComponent implements OnInit {

  constructor(
    private store: Store<AppState>,
    private sampleViewActions: SampleViewActions
  ) { }

  ngOnInit() {
  }

  close() {
    this.store.dispatch(this.sampleViewActions.hideSampleView());
  }
}
