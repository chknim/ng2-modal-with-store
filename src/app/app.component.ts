import {
  Component,
  ComponentFactoryResolver,
  ViewChild
} from '@angular/core';

import { AppState } from './reducers';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { SampleViewActions } from './components/sample-view/sample-view.actions';

import { ModalComponent } from './core/modal/modal.component';
import { SampleViewComponent } from './components/sample-view/sample-view.component';
import { SampleViewHeaderComponent } from './components/sample-view/sample-view-header.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  entryComponents: [
    SampleViewComponent,
    SampleViewHeaderComponent
  ]
})

export class AppComponent {
  @ViewChild(ModalComponent)
  public readonly modal: ModalComponent;

  // Actions
  sampleViewActions: SampleViewActions;

  // Modal-related parameters
  modalTitle: string;
  modalTitleIcon: string;
  headerFactory: any;
  bodyFactory: any;

  // State of the modal
  showSampleView: boolean = false;
  showSampleView$: Observable<boolean>;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private store: Store<AppState>
  ) {
    this.sampleViewActions = new SampleViewActions();
  }

  ngOnInit() {
    // Bind the proper scope
    this.closeModal = this.closeModal.bind(this);

    // Listening to sample view dialog
    this.showSampleView$ = this.store.select(state => state.sampleView.showSampleView);
    this.showSampleView$.subscribe(showSampleView => {
      this.showSampleView = showSampleView;
      if (this.showSampleView) {
        this.modal.show();
      } else {
        this.modal.hide();
      }
    });
  }

  launchModalWithHeaderComponent() {
    this.headerFactory = this.componentFactoryResolver.resolveComponentFactory(SampleViewHeaderComponent); 
    this.bodyFactory = this.componentFactoryResolver.resolveComponentFactory(SampleViewComponent);
    this.store.dispatch(this.sampleViewActions.showSampleView());
  }

  launchModalWithHeaderText() {
    this.modalTitle = 'Default header with customizable text';
    delete this.headerFactory;
    this.bodyFactory = this.componentFactoryResolver.resolveComponentFactory(SampleViewComponent);
    this.store.dispatch(this.sampleViewActions.showSampleView());
  }

  closeModal() {
    if (this.showSampleView) {
      this.store.dispatch(this.sampleViewActions.hideSampleView());
    }
  }
}

