// Barrelling is declared in case of specific component import.
export * from './modal.component';
export * from './modal.header';
export * from './modal.body';
export * from './modal.footer';
