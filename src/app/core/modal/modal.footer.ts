import { Component, Input, OnInit, ViewContainerRef, OnChanges } from '@angular/core';

@Component({
  selector: 'modal-footer',
  template: ``
})
export class ModalFooterComponent implements OnInit, OnChanges {
  @Input("factory") factory: any;
  private ref: any;
  constructor(private viewContainerRef: ViewContainerRef) {
  }

  ngOnInit() {
    this.loadFooter();
  }

  ngOnChanges() {
    this.loadFooter();
  }

  loadFooter() {
    if (this.ref) {
      this.ref.destroy();
    }
    if (this.factory) {
      this.ref = this.viewContainerRef.createComponent(this.factory);
      this.ref.changeDetectorRef.detectChanges();
    }
  }
}
