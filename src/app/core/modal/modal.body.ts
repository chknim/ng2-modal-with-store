import { Component, Input, OnInit, ViewContainerRef, OnChanges } from '@angular/core';

@Component({
  selector: 'modal-body',
  template: `<div *ngIf="!factory">{{title}}</div>`
})
export class ModalBodyComponent implements OnInit, OnChanges {

  @Input("title") title: string;
  @Input("factory") factory: any;
  private ref: any;
  constructor(private viewContainerRef: ViewContainerRef) {
  }

  ngOnInit() {
    this.loadBody();
  }

  ngOnChanges() {
    this.loadBody();
  }

  loadBody() {
    if (this.ref) {
      this.ref.destroy();
    }
    if (this.factory) {
      this.ref = this.viewContainerRef.createComponent(this.factory);
      this.ref.changeDetectorRef.detectChanges();
    }
  }
}
