import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ModalComponent } from './modal.component';
import { ModalHeaderComponent } from './modal.header';
import { ModalBodyComponent } from './modal.body';
import { ModalFooterComponent } from './modal.footer';

@NgModule({
  declarations: [
    ModalComponent,
    ModalHeaderComponent,
    ModalBodyComponent,
    ModalFooterComponent
  ],
  imports: [
    BrowserModule
  ],
  exports: [
    ModalComponent,
    ModalHeaderComponent,
    ModalBodyComponent,
    ModalFooterComponent
  ]
})
export class ModalModule {}
