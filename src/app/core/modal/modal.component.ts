import { Component, HostBinding, HostListener, Input, OnInit } from '@angular/core';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less']
})
export class ModalComponent implements OnInit {

  // Header
  @Input('title') modalTitle: string;
  @Input('titleIcon') modalTitleIcon: string;
  @Input('headerFactory') headerFactory: any;
  @Input('bodyFactory') bodyFactory: any;
  @Input('onClose') closeCallback: Function;

  @Input() animation: boolean = true;
  @Input() backdrop: string | boolean = true;

  @HostBinding('class.visible') get fadeClass(): boolean {
    return this.visible;
  }
  @HostListener('click', ['$event.target']) onClick(target: any) {
    if (target.nodeName.toLowerCase() === "modal") {
      this.hide();
    }
  }

  public visible = false;
  // todo: to allow animation
  private visibleAnimate = false;

  constructor() {}

  ngOnInit() {}

  public show(): void {
    // todo: this needs to be able to dispatch to the right action/store
    // todo: showing dialog should dispatch action to lock scroll on main or background
    this.visible = true;
    setTimeout(() => this.visibleAnimate = true);
  }

  public hide(): void {
    // todo: this needs to be able to dispatch to the right action/store
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false);
  }

  public closeNotified(): void { 
    if (this.closeCallback) {
      // closeCallback is a callback passed by its user allowing the upper application
      // to feed this event to Ngrx properly
      this.closeCallback();
    } else {
      // If closeCallback is not declared, regular event emitter mechanism kicks in.
      this.hide();
    }
  }
}
