import { Component, Input, Output, EventEmitter, OnInit, ViewContainerRef, OnChanges } from '@angular/core';

@Component({
  selector: 'modal-header',
  templateUrl: './modal.header.html',
  styleUrls: ['./modal.header.less']
})
export class ModalHeaderComponent implements OnInit, OnChanges {

  @Input("title") title: string;
  @Input("titleIcon") titleIcon: string;
  @Input("factory") factory: any;

  @Output("onClose") onClose: EventEmitter<string> = new EventEmitter<string>();

  private ref: any;
  constructor(private viewContainerRef: ViewContainerRef) {
  }

  ngOnInit() {
    this.loadHeader();
  }

  ngOnChanges() {
    this.loadHeader();
  }

  loadHeader() {
    if (this.ref) {
      this.ref.destroy();
    }
    if (this.factory) {
      this.ref = this.viewContainerRef.createComponent(this.factory);
      this.ref.changeDetectorRef.detectChanges();
    }
  }

  close(event: any) {
    this.onClose.emit('close');
  }
}
