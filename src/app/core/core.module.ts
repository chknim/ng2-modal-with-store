import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ModalComponent, ModalHeaderComponent, ModalBodyComponent, ModalFooterComponent } from './modal';

import { APP_IMPORTS } from '../app.imports';

@NgModule({
  declarations: [
    ModalComponent,
    ModalHeaderComponent,
    ModalBodyComponent,
    ModalFooterComponent
  ],
  imports: [
    APP_IMPORTS,
    BrowserModule
  ],
  exports: [
    ModalComponent,
    ModalHeaderComponent,
    ModalBodyComponent,
    ModalFooterComponent
  ]
})
export class CoreModule {}
