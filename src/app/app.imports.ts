import { RouterModule, PreloadAllModules } from '@angular/router';

import { StoreModule } from '@ngrx/store';

import { rootReducer } from './reducers';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ReactiveFormsModule } from '@angular/forms';

export const APP_IMPORTS = [
  NgbModule.forRoot(),
  ReactiveFormsModule,
  StoreModule.provideStore(rootReducer)
];
